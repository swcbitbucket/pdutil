"""
A collaboration - rkni+swhi
"""

import pandas as pd


def return_row_after(df: pd.DataFrame, col: str, val: any) -> tuple:
    """
    Take a df with a sorted col and return the row where col.val > val
    NOTE: May return None
    """
    for row in df.itertuples():
        if getattr(row, col) > val:
            return row


def return_row_before(df: pd.DataFrame, col: str, val: any) -> tuple:
    """
    Take a df with a sorted col and return the row where col.val < val
    """
    result = None
    for row in df.itertuples():
        if getattr(row, col) >= val:
            return result
        result = row


def return_row(df: pd.DataFrame, col: str, val: any) -> tuple:
    """
    Take a df with a col and return the first row where col.val = val
    NOTE: May return None
    """
    for row in df.itertuples():
        if getattr(row, col) == val:
            return row


def trim_timeline(df: pd.DataFrame, time_col: str) -> pd.DataFrame:
    """
    Take a df with a time_col and return a new df.
    The df will be sorted using time_col.
    New df should include only the rows where there is change in any of the columns.
    The first and last row will always be included.
    """
    df = df.sort_values(by=time_col)  # make sure we are sorted
    cols = df.columns.to_list()  # we need all columns except time column
    cols.remove(time_col)  # remove in place
    return changed_rows(df, cols, keep_last_row=True)  # call changed_rows to get the answer we want


def changed_rows(df: pd.DataFrame, cols: list = None, keep_last_row: bool = False) -> pd.DataFrame:
    """
    Take an ordered df and a list of columns.
    Return a df where all rows have at least one col change.
    Optional preserve last row even if not changed.
    Useful to filter a large dataset to just capture the changes.
    """
    if isinstance(df, pd.Series):
        # if a series has been passed, convert it to a dataframe for handling
        cols = None
        df = df.to_frame()
    if isinstance(cols, str):
        cols = [cols]

    filter_cols = df[cols] if cols else df
    nulls = df.isnull()
    still_null = nulls & nulls.shift(1).fillna(False)

    # We find where the rows have changed but we need to remove the false positives
    # caused by nulls
    interesting_rows = (filter_cols != filter_cols.shift(1)) & ~still_null
    # optional retain last row
    if keep_last_row:
        interesting_rows.iloc[-1] = True
    return df[interesting_rows.any(axis=1)]


def add_episode_col(df: pd.DataFrame, group_col: str, dt_col: str, secs_treshold: float = 180.0) -> pd.DataFrame:
    """
    this finds gaps in a datetime field representing different episodes in a dataframe
    data is grouped by group_col, episode column is added with points for same
    ident below threshold given the same episode id
    """
    assert (group_col in df.columns and dt_col in df.columns)

    df_sorted = df.sort_values([group_col, dt_col])
    delta_seconds = (df_sorted[dt_col] - df_sorted[dt_col].shift(1)).dt.total_seconds()

    episode_chngs = ((df_sorted[group_col] != df_sorted[group_col].shift(1)) | (delta_seconds >= secs_treshold))

    df_sorted['episodes'] = episode_chngs.cumsum()

    return df_sorted


def get_df_invariants(df: pd.DataFrame) -> (pd.Series, pd.DataFrame):
    """
    Return a pandas seriers of the invariant columns in the dataframe and
    ad dataframe filtered to just variant columns
    """
    if df.empty:
        return pd.Series(dtype='object'), pd.DataFrame()

    invariants = [col for col in df.columns if len(df[col].unique()) == 1]
    variants = [col for col in df.columns if col not in invariants]

    return df[invariants].iloc[0], df[variants]
