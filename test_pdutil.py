"""
"""
import datetime
import pandas as pd
import numpy as np

from pdutil import (changed_rows, trim_timeline, add_episode_col, get_df_invariants, 
                    return_row_after, return_row_before, return_row)


def timeline_df(N: int):
    """
    """
    data = []
    for row in range(N):
        data.append([datetime.datetime.combine(datetime.date.today(), datetime.time(hour=row)), row // 3, row // 5])
    result = pd.DataFrame(data, columns=['time', 'count', 'row'])
    print(result)
    return result


def test_changed_rows():
    """
    """
    df = timeline_df(24)
    result = changed_rows(df, ['count', 'row'])
    print(result)
    assert result.shape == (11, 3)  # no last row


def test_trim_timeline():
    """
    """
    df = timeline_df(24)
    result = trim_timeline(df, 'time')
    print(result)
    assert result.shape == (12, 3)  # includes last row


def test_trim_timeline_2():
    """
    df len = 2
    """
    df = timeline_df(2)
    result = trim_timeline(df, 'time')
    print(result)
    assert result.shape == (2, 3)  # includes first and last row


def test_trim_timeline_1():
    """
    df len = 1
    """
    df = timeline_df(1)
    result = trim_timeline(df, 'time')
    print(result)
    assert result.shape == (1, 3)  # includes first and last row


def test_nulls():
    """
    """
    df = pd.DataFrame({'Col1': [10, 10, 15, 15, 45],
                       'Col2': [13, 13, 15, 15, 45],
                       'Col3': ['None', 'None', 'None', 'None', 'None'],
                       'Col4': [np.nan, np.nan, np.nan, np.nan, np.nan],
                       'Col5': [None, None, None, None, None],
                       'Col6': [pd.NaT, pd.NaT, pd.NaT, pd.NaT, pd.NaT]})
    print(df)
    df = changed_rows(df)
    print(df)
    assert df.shape == (3, 6)


def test_2nans():
    """
    """
    df = pd.DataFrame({'Col1': [10, None, None, 15, 15],
                       'Col2': [23, 23, 23, 23, 23],
                       'Col3': [17, 17, 17, 17, 17]})
    print(df)
    df = changed_rows(df)
    print(df)
    assert df.shape == (3, 3)


def test_single_nans():
    """
    """
    df = pd.DataFrame({'Col1': [10, None, 15, None, 45],
                       'Col2': [23, 23, 23, 23, 23],
                       'Col3': [17, 17, 17, 17, 17]})
    print(df)
    df = changed_rows(df)
    print(df)
    assert df.shape == (5, 3)


def test_rkni():
    """
    """
    df = pd.DataFrame({'Col1': [10, 10, 15, 15, 45],
                       'Col2': [13, 23, 18, 33, 48],
                       'Col3': [17, 17, 17, 17, 17]})

    assert df.equals(changed_rows(df))
    assert [0, 2, 4] == list(changed_rows(df, 'Col1').index)
    assert [0] == list(changed_rows(df, 'Col3').index)

    assert [0, 2, 4] == list(changed_rows(df, ['Col1', 'Col3']).index)
    assert list(df.index) == list(changed_rows(df, ['Col1', 'Col3', 'Col2']).index)

    assert [0, 2, 4] == list(changed_rows(df['Col1'], 'Col1').index), changed_rows(df['Col1'], 'Col1')
    assert df['Col2'].to_frame().equals(changed_rows(df['Col2'])), (df['Col2'], changed_rows(df['Col2']))


def single_flt_df(flight_ident='BAW123', samples_per_ep=100, dt_col='ts_time', flt_ident_col='ts_callsign'):
    dt_a = pd.date_range('2018-01-01 09:00', periods=samples_per_ep, freq='6S', name=dt_col).to_series()
    dt_b = pd.date_range('2018-01-01 10:00', periods=samples_per_ep, freq='6S', name=dt_col).to_series()
    dt_c = pd.date_range('2018-01-01 11:00', periods=samples_per_ep, freq='6S', name=dt_col).to_series()

    flt_df_a = pd.concat([dt_a, dt_b, dt_c]).to_frame().reset_index(drop=True)

    flt_df_a[flt_ident_col] = flight_ident
    return flt_df_a


def sample_flts_df(samples_per_ep=100, dt_col='ts_time', flt_ident_col='ts_callsign'):
    """
    make a sample flt df with 4 flights each with 3 episodes
    """
    flt_df_a = single_flt_df('BAW123', samples_per_ep, dt_col, flt_ident_col)
    flt_df_b = flt_df_a.copy(deep=True)
    flt_df_c = flt_df_a.copy(deep=True)
    flt_df_d = flt_df_a.copy(deep=True)

    flt_df_b[flt_ident_col] = 'ACA456'
    flt_df_c[flt_ident_col] = 'DAL678'
    flt_df_d[flt_ident_col] = 'UAL901'

    flt_df = pd.concat([flt_df_a, flt_df_b, flt_df_c, flt_df_d]).reset_index(drop=True)

    return flt_df


def test_episodes_added():
    """
    """
    threshold_seconds = 180
    dt_col = 'ts_time'
    flt_ident_col = 'ts_callsign'

    flt_df = add_episode_col(single_flt_df(), flt_ident_col, dt_col, threshold_seconds)

    assert 'episodes' in flt_df.columns
    assert len(flt_df.episodes.unique()) == 3


def test_episode_multiple():
    """
    """
    samples_per_ep = 100
    threshold_seconds = 180
    dt_col = 'ts_time'
    flt_ident_col = 'ts_callsign'

    flt_df = add_episode_col(sample_flts_df(), flt_ident_col, dt_col, threshold_seconds)

    for grp_id, grp in flt_df.groupby([flt_ident_col, 'episodes']):
        srt_grp = grp.sort_values(dt_col)
        assert ((srt_grp[dt_col] - srt_grp[dt_col].shift(-1)).dropna().dt.total_seconds() < threshold_seconds).all()  # check all gaps smaller than threshold
        assert srt_grp.shape == (samples_per_ep, 3)  # each group contains 100 points
        assert srt_grp.iloc[0][dt_col].minute == 0 and srt_grp.iloc[0][dt_col].second == 0  # each group starts on the hour


def test_no_invariant():
    """
    """
    data = {'var1': [1, 2, 3, 4, 5, np.nan, 7, 8, 9],
            'var2': ['Order', np.nan, 'Inv', 'Order', 'Order', 'Shp', 'Order', 'Order', 'Inv'],
            'var3': [101, 101, 101, 102, 102, 102, 103, 103, np.nan],
            'var4': [np.nan, 1, 1, 1, 1, 1, 1, 1, 1],
            'var5': [1, 2, 3, 4, 5, 6, 7, 8, 9]}

    df = pd.DataFrame(data)

    inv_df, var_df = get_df_invariants(df)

    assert isinstance(inv_df, pd.Series)
    assert isinstance(var_df, pd.DataFrame)

    assert inv_df.empty, inv_df
    assert var_df.equals(df), var_df


def test_no_variants():
    """
    """
    data = {'var5': [1, 1, 1, 1, 1, 1, 1, 1, 1],
            'var6': [np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
            'var7': ["a", "a", "a", "a", "a", "a", "a", "a", "a"]}

    df = pd.DataFrame(data)

    inv_df, var_df = get_df_invariants(df)
    print(inv_df)
    print(var_df)

    assert isinstance(inv_df, pd.Series)
    assert isinstance(var_df, pd.DataFrame)

    assert inv_df.equals(df.iloc[0])
    assert var_df.empty


def test_empty_df():
    """
    """
    df = pd.DataFrame()

    inv_df, var_df = get_df_invariants(df)

    assert isinstance(inv_df, pd.Series)
    assert isinstance(var_df, pd.DataFrame)

    assert inv_df.empty
    assert var_df.empty


def test_mixed_invariant_variant():
    """
    """
    data = {'var1': [1, 2, 3, 4, 5, np.nan, 7, 8, 9],
            'var2': ['Order', np.nan, 'Inv', 'Order', 'Order', 'Shp', 'Order', 'Order', 'Inv'],
            'var3': [101, 101, 101, 102, 102, 102, 103, 103, np.nan],
            'var4': [np.nan, 1, 1, 1, 1, 1, 1, 1, 1],
            'var5': [1, 1, 1, 1, 1, 1, 1, 1, 1],
            'var6': [np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
            'var7': ["a", "a", "a", "a", "a", "a", "a", "a", "a"],
            'var8': [1, 2, 3, 4, 5, 6, 7, 8, 9]}

    df = pd.DataFrame(data)

    inv_df, var_df = get_df_invariants(df)
    print(inv_df)
    print(var_df)

    assert isinstance(inv_df, pd.Series)
    assert isinstance(var_df, pd.DataFrame)

    assert set(inv_df.index) == {'var5', 'var6', 'var7'}
    assert set(var_df.columns) == {'var1', 'var2', 'var3', 'var4', 'var8'}


def test_row_after():
    """
    """
    data = dict(time=[-2, -1, 0, 1, 2],
                value=['m2', 'm1', 'z', 'p1', 'p2'])
    df = pd.DataFrame(data)
    assert return_row_after(df, 'time', 0).value == 'p1'
    assert return_row_after(df, 'time', -2).value == 'm1'
    assert return_row_after(df, 'time', 2) is None
    assert return_row_after(df, 'time', 666) is None


def test_row_before():
    """
    """
    data = dict(time=[-2, -1, 0, 1, 2],
                value=['m2', 'm1', 'z', 'p1', 'p2'])
    df = pd.DataFrame(data)
    assert return_row_before(df, 'time', -2) is None
    assert return_row_before(df, 'time', 0).value == 'm1'
    assert return_row_before(df, 'time', -1).value == 'm2'
    assert return_row_before(df, 'time', 2).value == 'p1'


def test_row():
    """
    """
    data = dict(time=[-2, -1, 0, 1, 2],
                value=['m2', 'm1', 'z', 'p1', 'p2'])
    df = pd.DataFrame(data)
    assert return_row(df, 'time', -2).value == 'm2'
    assert return_row(df, 'time', 0).value == 'z'
    assert return_row(df, 'time', 2).value == 'p2'
    assert return_row(df, 'time', 666) is None
